/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package colecciones;

import java.util.Iterator;

/**
 *
 * @author estudiante
 */
public class ListaCD<T> implements Iterable<T> {

    private NodoD<T> cabeza;
    private int tamanio;

    public ListaCD() {

        this.cabeza = new NodoD();
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        //sobra:
        this.cabeza.setInfo(null);

    }

    public void insertarInicio(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza.getSig(), this.cabeza);
        //redireccionar
        this.cabeza.getSig().setAnt(nuevo);
        this.cabeza.setSig(nuevo);

        this.tamanio++;
    }

    public void insertarFinal(T info) {
        NodoD<T> nuevo = new NodoD<T>(info, this.cabeza, this.cabeza.getAnt());
        this.cabeza.setAnt(nuevo);
        nuevo.getAnt().setSig(nuevo);
        this.tamanio++;
    }

    public T eliminar(int i) throws Exception {
        if (i >= this.tamanio || this.esVacio() || i < 0) {
            throw new Exception("no se puede eliminar");
        }

        NodoD<T> n = this.cabeza.getSig();

        while (i > 0) {
            n = n.getSig();
            i--;
        }
        NodoD<T> aux = n;
        n.getAnt().setSig(n.getSig());
        n.getSig().setAnt(n.getAnt());
        this.tamanio--;
        return n.getInfo();
    }

    /* public T eliminar(int i) throws Exception {
        if (i >= this.tamanio || this.esVacio() || i < 0) {
            throw new Exception("no se puede eliminar");
        }

        NodoD<T> n = this.cabeza.getSig();
        
        for (int j = 0; j <= i; j++) {
            
            if (j == i) {
                NodoD<T> aux=n;
                n.getAnt().setSig(n.getSig());
                n.getSig().setAnt(n.getAnt());
                this.tamanio--;
                return n.getInfo();
            }

            n = n.getSig();
        }
        return null;
    }*/
    
    public void concatenarLista(ListaCD<T> l2) {
        if (this.esVacio() && l2.esVacio()) {
            return;
        }

        if (l2.esVacio()) {
            return;
        } else if (this.esVacio()) {

            this.cabeza.setSig(l2.cabeza.getSig());//conecto el sig de la cabeza con el siguiente a la cabeza de l2
            l2.cabeza.getSig().setAnt(cabeza);//conecto el sig de la cabeza de l2 con la cabeza
            this.cabeza.setAnt(l2.cabeza.getAnt());//conecto la cabeza con el ultimo nodo de l2
            l2.cabeza.getAnt().setSig(cabeza);//conecto el ultimo nodo de l2 conl acabeza

            this.tamanio = l2.tamanio;
            l2.eliminarLista();
        }

        l2.cabeza.getAnt().setSig(this.cabeza.getSig());
        this.cabeza.getSig().setAnt(l2.cabeza.getAnt());
        this.cabeza.setSig(l2.cabeza.getSig());
        l2.cabeza.getSig().setAnt(this.cabeza);
        this.tamanio = this.tamanio + l2.tamanio;

        l2.eliminarLista();

    }

    public void eliminarLista() {
        this.cabeza.setSig(cabeza);
        this.cabeza.setAnt(cabeza);
        this.tamanio = 0;
    }

    public void concurrencia(ListaCD<T> l1,ListaCD<T> l2){
     //l1 Lista a buscar en this
     //l2 lista a reemplazar en this
        for(NodoD<T> t=this.cabeza.getSig();t!=cabeza;t=t.getSig())
            buscar(t,l1,l2);
    }
    
    private ListaCD copia(ListaCD<T> l1)
    {
        ListaCD<T> list=new ListaCD();
    for(NodoD<T> t=l1.cabeza.getSig();t!=cabeza;t=t.getSig())
    list.insertarFinal(t.getInfo());
    
    return list;
    }
    
private void buscar(NodoD<T> t,ListaCD<T> l1,ListaCD<T> l2){
     
    NodoD<T> n=t;
    NodoD<T> puntero=l1.cabeza.getSig();
    NodoD<T> ant=n;
    
    while(n!=this.cabeza && puntero!=l1.cabeza){
  
        if( n.getSig()!=cabeza && n.getSig().getInfo().equals(puntero.getInfo()) )
        ant=n;
            
        if(n.getInfo().equals(puntero.getInfo())){
            
            if(puntero.getSig()==l1.cabeza)
               { 
                   ListaCD<T> list=l2.copia(l2);
                   reemplazar(ant,n,list);
                }
            puntero=puntero.getSig();
            n=n.getSig();
        }
        else
        n=n.getSig();
    }
    
    }


    
    private void reemplazar(NodoD<T> an,NodoD<T> n,ListaCD<T> l2){
    NodoD<T> ant=an;
    NodoD<T> t=n;
  
    ant.setSig(l2.cabeza.getSig());
    l2.cabeza.getAnt().setSig(t.getSig());
    l2.cabeza.getSig().setAnt(ant);
    t.getSig().setAnt(l2.cabeza.getAnt());
    
    l2.eliminarLista();
    
    }    
    public boolean sonIguales(ListaCD<T> l2) throws Exception {
        if (this.esVacio() || l2.esVacio() || this.tamanio != l2.tamanio) {
            throw new Exception("lista vacia");
        }

        NodoD<T> y = l2.cabeza.getSig();
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig(), y = y.getSig()) {
            if (!x.getInfo().equals(y.getInfo())) {
                return false;
            }
        }
        return true;
    }

    public boolean elemtsRepetidos(ListaCD l2) throws Exception {
        if (this.esVacio() || l2.esVacio()) {
            throw new Exception("lista vacia");
        }

        for (NodoD<T> t = this.cabeza.getSig(); t != cabeza; t = t.getSig()) {
            if (contieneRepets(t, l2)) {
                return true;
            }
        }
        return false;
    }

    private boolean contieneRepets(NodoD<T> t, ListaCD<T> l2) {

        NodoD<T> n = l2.cabeza.getSig();
        NodoD<T> x = t;
        while (n != l2.cabeza) {
            if (x.getInfo().equals(n.getInfo())) {
                return true;
            }

            n = n.getSig();
        }

        return false;
    }

    public void eliminarElemtsRepetidos() {
        if (this.esVacio() || this.tamanio == 1) {
            return;
        }

        for (NodoD<T> t = this.cabeza.getSig(); t != this.cabeza; t = t.getSig()) {
            eliminarRepet(t);
        }
    }

    private void eliminarRepet(NodoD<T> t) {
        NodoD<T> elim = t;
        NodoD<T> puntero = t.getSig();

        while (puntero != cabeza) {
            if (elim.getInfo().equals(puntero.getInfo())) {
                puntero.getAnt().setSig(puntero.getSig());
                puntero.getSig().setAnt(puntero.getAnt());
                NodoD<T> aux = puntero;
                puntero = puntero.getSig();
                aux.setAnt(null);
                aux.setSig(null);
            } else {
                puntero = puntero.getSig();
            }
        }

    }

    public String toString() {
        String msg = "";
        for (NodoD<T> x = this.cabeza.getSig(); x != this.cabeza; x = x.getSig()) {
            msg += x.getInfo() + "<->";
        }
        return msg;
    }

    public boolean esVacio() {

        return (this.cabeza == this.cabeza.getSig());
        //return (this.cabeza==this.cabeza.getAnt());
        //return this.tamanio==0;
    }

    public int getTamanio() {
        return tamanio;
    }

    @Override
    public Iterator<T> iterator() {
        return new IteratorListaCD(this.cabeza);
    }

}
