package colecciones;

import java.util.Iterator;

/**
 *
 * @author estudiante
 */
public class IteratorListaCD<T> implements Iterator<T> {

    
    private NodoD<T> cabeza, sig;

    public IteratorListaCD(NodoD<T> cabeza) {
        //Lo dejo quieto
        this.cabeza = cabeza;
        //Es el que voy a mover
        this.sig=this.cabeza.getSig();
    }
    
    
    
    
    @Override
    public boolean hasNext() {
        return this.sig!=this.cabeza;
    }

    @Override
    public T next() {
        if(this.hasNext())
        {
            this.sig=this.sig.getSig();
            return this.sig.getAnt().getInfo();
        }
     return null;
    }
    
}

